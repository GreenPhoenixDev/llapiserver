﻿using MongoDB.Driver;
using UnityEngine;

public class Mongo
{
	private const string MONGO_URI = "mongodb+srv://Nackenmassage:testpassword@lobbydb-mmzo1.mongodb.net/test?retryWrites=true&w=majority";
	private const string DATABASE_NAME = "lobbydb";

	private MongoClient client;
	private IMongoDatabase db;

	private IMongoCollection<Model_Account> accountCollection;
	private IMongoCollection<Model_Follow> followCollection;

	public void Init()
	{
		client = new MongoClient(MONGO_URI);
		db = client.GetDatabase(DATABASE_NAME);

		//  This is where we would initialize collections
		accountCollection = db.GetCollection<Model_Account>("account");
		followCollection = db.GetCollection<Model_Follow>("follow");

		Debug.Log(" Database would have been initialized");
	}
	public void Shutdown()
	{
		client = null;
		db = null;
	}

	#region Insert
	//public bool InsertFollow(string token, string emailorUsername)
	//{
	//	Model_Follow newFollow = new Model_Follow();
	//	newFollow.Sender = new MongoDBRef("account", FindAccountByToken(token)._id);

	//	// Start by getting the reference to our follow
	//	if (!Utility.IsEmail(emailorUsername))
	//	{
	//		// If it´s a username/discriminator
	//		string[] data = emailorUsername.Split('#');
	//		if(data[1] != null)
	//		{
	//			Model_Account follow = FindAccountByUsernameAndDiscriminator(data[0], data[1]);
	//			if (follow != null)
	//				newFollow.Target = new MongoDBRef("account", follow._id);
	//		}
	//	}
	//	else
	//	{
	//		// If it´s an email
	//	}
	//}
	public bool InsertAccount(string username, string password, string email)
	{
		// Check if the email is valid
		if (!Utility.IsEmail(email))
		{
			Debug.Log($"{email} is not a email!");
			return false;
		}
		// Check if the username is valid
		if (!Utility.IsUsername(username))
		{
			Debug.Log($"{username} is not a username!");
			return false;
		}
		// Check if the account already exists
		if (FindAccountByEmail(email) != null)
		{
			Debug.Log($"{email} is already being used");
			return false;
		}

		Model_Account newAccount = new Model_Account();
		newAccount.Username = username;
		newAccount.ShaPassword = password;
		newAccount.Email = email;
		newAccount.Discriminator = "0000";

		// Roll for unique Discriminator
		int rollCount = 0;
		while (FindAccountByUsernameAndDiscriminator(newAccount.Username, newAccount.Discriminator) != null)
		{
			newAccount.Discriminator = Random.Range(0, 9999).ToString("0000");

			rollCount++;
			if (rollCount > 1000)
			{
				Debug.Log("We rolled too many times, suggest username change");
				return false;
			}
		}

		accountCollection.InsertOne(newAccount);

		return true;
	}
	public Model_Account LoginAccount(string usernameOrEmail, string password, int cnnId, string token)
	{
		Model_Account myAccount = null;
		FilterDefinition<Model_Account> filter = null;

		// Find my account
		if (Utility.IsEmail(usernameOrEmail))
		{
			// If i logged in using my email

			var builder = Builders<Model_Account>.Filter;

			filter = builder.And(
				builder.Eq(u => u.Email, usernameOrEmail), 
				builder.Eq(u => u.ShaPassword, password));

			myAccount = accountCollection.Find(filter).Single();

		}
		else
		{
			// If i logged in using username#discriminator
			string[] data = usernameOrEmail.Split('#');
			if (data[1] != null)
			{
				var builder = Builders<Model_Account>.Filter;

				filter = builder.And(
					builder.Eq(u => u.Email, data[0]),
					builder.Eq(u => u.Discriminator, data[1]),
					builder.Eq(u => u.ShaPassword, password));

				myAccount = accountCollection.Find(filter).SingleOrDefault();
			}
		}

		if(myAccount != null)
		{
			// We found the account, let´s log in
			myAccount.ActiveConnection = cnnId;
			myAccount.Token = token;
			myAccount.Status = 1;
			myAccount.LastLogin = System.DateTime.Now;

			var update = Builders<Model_Account>.Update.
				Set("myAccount.ActiveConnection", cnnId).
				Set("myAccount.Token", token).
				Set("myAccount.Status", 1).
				Set("myAccount.LastLogin", System.DateTime.Now);

			accountCollection.UpdateOne(filter, update);
		}
		else
		{
			// Didn´t find anything, invalod credentials
		}

		return myAccount;
	}
	#endregion

	#region Fetch 
	public Model_Account FindAccountByEmail(string email)
	{
		return accountCollection.Find(u => u.Email.Equals(email)).SingleOrDefault();
	}
	public Model_Account FindAccountByUsernameAndDiscriminator(string username, string discriminator)
	{
		var builder = Builders<Model_Account>.Filter;

		var filter = builder.And(
			builder.Eq(u => u.Username, username),
			builder.Eq(u => u.Discriminator, discriminator));

		return accountCollection.Find(filter).SingleOrDefault();
	}
	public Model_Account FindAccountByToken(string token)
	{
		return accountCollection.Find(u => u.Token.Equals(token)).SingleOrDefault();
	}
	#endregion

	#region Update
	#endregion

	#region Delete
	#endregion
}
