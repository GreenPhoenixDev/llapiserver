## LLAPI Server - Paused

### Purpose

 With this project I wanted to get a general idea of how low-level networking in Unity works.
 So I started watching a tutorial series by N3K EN.
 
 https://www.youtube.com/watch?v=9DUCCWFG5cU&list=PLLH3mUGkfFCVSV6Q1UJE7Ge6UfhGzRAnH&index=1 
 
 I watched the series until video 8/9, when in the middle of programming i just couldn't login to the server anymore. 
 I started debugging a lot and found the issue, but it does not lie in my code directly, it has to do with the AtlasDB database manager I use.
 I've tracked the bug down to the exact line, where it breaks, but i was not able to find a solution, due to being new to database management.
 
 So currently this project is on hold, but I'm looking forward to fixing it soon.
 
### LLAPI Client

Here is the client side of the project, but it does not contain a README.

https://gitlab.com/GreenPhoenixDev/llapiclient 

### Image

On the left side of the picture is the lobby scene of the client and on the right side is the server scene (empty exept for a server script).

![Imgur](https://i.imgur.com/YVOuntH.png)